'use strict';

/**
 * Module dependencies
 */

// Node.js core.
const crypto = require('crypto');

// Public node modules.
const _ = require('lodash');
const anchor = require('anchor');
const nodemailer = require('nodemailer'); 

/**
 * Auth controller
 */

module.exports = {

  /**
   * Main action for login
   * both for local auth and provider auth.
   */

  callback: function * () {
    const ctx = this;

    const provider = ctx.params.provider || 'local';
    const params = ctx.request.body;
    const access_token = ctx.query.access_token;

    if (provider === 'local') {
      // The identifier is required.
      if (!params.identifier) {
        ctx.status = 400;
        return ctx.body = {
          message: 'Please provide your username or your e-mail.'
        };
      }

      // The password is required.
      if (!params.password) {
        ctx.status = 400;
        return ctx.body = {
          message: 'Please provide your password.'
        };
      }

      const query = {};

      // Check if the provided identifier is an email or not.
      const isEmail = !anchor(params.identifier).to({
        type: 'email'
      });

      // Set the identifier to the appropriate query field.
      if (isEmail) {
        query.email = params.identifier;
      } else {
        query.username = params.identifier;
      }

      // Check if the user exists.
      try {
        const user = yield User.findOne(query);

        let roles = yield User.findOne(query)
        .populate("roles");//, {select:["name"]}); if want to custom select

        if (!user) {
          ctx.status = 403;
          return ctx.body = {
            message: 'Identifier or password invalid.'
          };
        }

        // The user never registered with the `local` provider.
        if (!user.password) {
          ctx.status = 400;
          return ctx.body = {
            message: 'This user never set a local password, please login thanks to the provider used during account creation.'
          };
        }

        const validPassword = user.validatePassword(params.password);

        if (!validPassword) {
          ctx.status = 403;
          return ctx.body = {
            message: 'Identifier or password invalid.'
          };
        } else {
          ctx.status = 200;
          ctx.body = {
            jwt: strapi.api.user.services.jwt.issue(user),
            user: user,
            roles: roles.roles[0]
          };
        }
      } catch (err) {
        ctx.status = 500;
        return ctx.body = {
          message: err.message
        };
      }
    } else {
      // Connect the user thanks to the third-party provider.
      try {
        const user = yield strapi.api.user.services.grant.connect(provider, access_token);

        ctx.redirect(strapi.config.frontendUrl || strapi.config.url + '?jwt=' + strapi.api.user.services.jwt.issue(user) + '&user=' + JSON.stringify(user));
      } catch (err) {
        ctx.status = 500;
        return ctx.body = {
          message: err.message
        };
      }
    }
  },

  /**
   * Register endpoint for local user.
   */

  register: function * () {
    const ctx = this;
    const params = _.assign(ctx.request.body, {
      id_ref: 1,
      lang: strapi.config.i18n.defaultLocale,
      template: 'default',
      provider: 'local'
    });

    // Password is required.
    if (!params.password) {
      ctx.status = 400;
      return ctx.body = {
        message: 'Invalid password field.'
      };
    }

    // Throw an error if the password selected by the user
    // contains more than two times the symbol '$'.
    if (strapi.api.user.services.user.isHashed(params.password)) {
      ctx.status = 400;
      return ctx.body = {
        message: 'Your password can not contain more than three times the symbol `$`.'
      };
    }

    var manager = Role.find()
    manager.where({'id':'5a040a9ffd8c37d41a61117a'})  
    manager.populate('users') 
    manager = yield manager;  
    
    var manager2 = "";
    for(var c=0; c<=manager.length; c++){
      manager2 = manager2 + manager[0]['users'][c]['email'] + ',';
    }
    console.log(manager2)

    // First, check if the user is the first one to register.
    try {
      const usersCount = yield User.count();

      // Create the user
      let user = yield User.create(params);

      // Check if the user is the first to register
      if (usersCount === 0) {
        // Find the roles
        const roles = yield Role.find();

        // Add the role `admin` to the current user
        user.roles.add(_.find(roles, {name: 'admin'}));

        // Prevent double encryption.
        delete user.password;

        user = yield user.save();

      }

      let mailOptions2 = "";
      if(params.roles.id == '5a040a85fd8c37d41a611176')
      {
        let mailOptions2 = {
          from: 'SkyAce', // sender address
          headers : 'SkyAce',
          // to: 'akatgelar@gmail.com', // list of receivers
          subject: 'Welcome to SkyAce! ', // Subject line
          to: user.email, // list of receivers
          cc: manager2,
          // text: 'Hello world?', // plain text body
          // html: 'This is your activation link <b><a>localhost:8181/auth/activationLink/123</a></b>' // html body
          html:   '<br>' +
                  'Dear '+ params.first_name + ' ' + params.last_name + ',' +
                  '<br><br>'+
                  'Thank you for signing up with SkyAce!' +
                  '<br><br>' +
                  'SkyAce provides an opportinity for everyone, old and young, to enrich themselves and acquire new knowledge on the go.'+
                  '<br>'+
                  'It\'s our pleasure to have you with us and we can\'t wait for you to start enjoying our services!' +
                  '<br><br>' +
                  'Please activate your account below.' +
                  '<br>' +
                  '<b><a href="http://skyace.ligarian.com/Auth/ActivateAccount/'+user.id+'">Activate My Account</a></b>' +
                  '<br><br>' +
                  'If you have any questions or experience difficulties while using our services, please email us at <b>studentcare@skyace.com</b>.' +
                  '<br>'+
                  'You can also visit our website for futher information.'+
                  '<br><br>' +
                  'From Team SkyAce'+
                  '<br>'+
                  '<b><a href="http://skyace.ligarian.com/">skyace.com</a></b>' +
                  '<br><br>'
        };

        console.log(mailOptions2);
        try {
          // Generate test SMTP service account from ethereal.email
          // Only needed if you don't have a real mail account for testing
          //nodemailer.createTestAccount((err, account) => {

              // create reusable transporter object using the default SMTP transport
              let transporter = nodemailer.createTransport({
                  host: strapi.api.email.config.smtp.service.host,
                  port: strapi.api.email.config.smtp.service.port,
                  secure: strapi.api.email.config.smtp.service.secure, 
                  auth: {
                    user: strapi.api.email.config.smtp.service.user,
                    pass: strapi.api.email.config.smtp.service.pass
                  },
                  tls: { 
                      rejectUnauthorized: false 
                  }
              });
    
              // send mail with defined transport object
              transporter.sendMail(mailOptions2, (error, info) => {
                  if (error) {
                      console.log("ada error ::: " + error);
                      return console.log(error);
                  }
                  console.log('Message sent: %s', info.messageId);
                  // Preview only available when sending through an Ethereal account
                  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
                  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
              });
          //});
        } catch (err) {
          this.status = err && err.status || 500;
          this.body = err;
          console.log(err);
        }
      }
      else if(params.roles.id == '5a040a8dfd8c37d41a611178')
      {
        let mailOptions2 = {
          from: 'SkyAce', // sender address
          headers : 'SkyAce',
          // to: 'akatgelar@gmail.com', // list of receivers
          subject: 'Welcome to SkyAce! ', // Subject line
          to: user.email, // list of receivers
          cc : manager2,
          // text: 'Hello world?', // plain text body
          // html: 'This is your activation link <b><a>localhost:8181/auth/activationLink/123</a></b>' // html body
          html:   '<br>'+
                  'Dear '+ params.first_name + ' ' + params.last_name + ',' +
                  '<br><br>'+
                  'Thank you for signing up with SkyAce!' +
                  '<br><br>' +
                  'SkyAce provides an opportinity for people with a passion for teaching to connect with student all over the world.'+
                  '<br>'+
                  'Teaching with SkyAce gives you the flexibility and convenience to teach from the comfort of your home!' +
                  '<br>'+
                  'It\'s our pleasure to have you with us and we can\'t wait for you to start enjoying our services!' +
                  '<br><br>' +
                  'Please activate your account below.' +
                  '<br>' +
                  '<b><a href="http://skyace.ligarian.com/Auth/ActivateAccount/'+user.id+'">Activate My Account</a></b>' +
                  '<br><br>' +
                  'If you have any questions or experience difficulties while using our services, please email us at <b>studentcare@skyace.com</b>.' +
                  '<br>'+
                  'You can also visit our website for futher information.'+
                  '<br><br>' +
                  'From Team SkyAce'+
                  '<br>'+
                  '<b><a href="http://skyace.ligarian.com/">skyace.com</a></b>' +
                  '<br><br>'
        };

        console.log(mailOptions2);
        try {
          // Generate test SMTP service account from ethereal.email
          // Only needed if you don't have a real mail account for testing
          //nodemailer.createTestAccount((err, account) => {

              // create reusable transporter object using the default SMTP transport
              let transporter = nodemailer.createTransport({
                  host: strapi.api.email.config.smtp.service.host,
                  port: strapi.api.email.config.smtp.service.port,
                  secure: strapi.api.email.config.smtp.service.secure, 
                  auth: {
                    user: strapi.api.email.config.smtp.service.user,
                    pass: strapi.api.email.config.smtp.service.pass
                  },
                  tls: { 
                      rejectUnauthorized: false 
                  }
              });
    
              // send mail with defined transport object
              transporter.sendMail(mailOptions2, (error, info) => {
                  if (error) {
                      console.log("ada error ::: " + error);
                      return console.log(error);
                  }
                  console.log('Message sent: %s', info.messageId);
                  // Preview only available when sending through an Ethereal account
                  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
                  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
              });
          //});
        } catch (err) {
          this.status = err && err.status || 500;
          this.body = err;
          console.log(err);
        }
      }
 
      ctx.status = 200;
      ctx.body = {
        jwt: strapi.api.user.services.jwt.issue(user),
        user: user
      };
    } catch (err) {
      ctx.status = 500;
      return ctx.body = {
        message: err.message
      };
    }
  },

  /**
   * Logout endpoint to disconnect the user.
   */

  logout: function * () {
    this.session = {};
    this.body = {};
  },

  /**
   * Send link to change user password.
   * Generate token to make change password action.
   */

  forgotPassword: function * () {
    const email = this.request.body.email;
    const url = this.request.body.url || strapi.config.url;
    let user;

    try {
      // Find the user user thanks to his email.
      user = yield User.findOne({email: email});

      // User not found.
      if (!user) {
        this.status = 400;
        return this.body = {
          message: 'This email does not exist.'
        };
      }
    } catch (err) {
      this.status = 500;
      return this.body = {
        message: err.message
      };
    }

    // Generate random token.
    const resetPasswordToken = crypto.randomBytes(64).toString('hex');

    // Set the property code of the local passport.
    user.resetPasswordToken = resetPasswordToken;

    // Update the user.
    try {
      user = yield user.save();
    } catch (err) {
      this.status = 500;
      return this.body = {
        message: err.message
      };
    }

    // Send an email to the user.
    let mailOptions2 = {
          from: 'elearningnet.sg@gmail.com', // sender address
          // to: 'akatgelar@gmail.com', // list of receivers
          to: user.email, // list of receivers
          subject: 'Activation ', // Subject line
          // text: 'Hello world?', // plain text body
          // html: 'This is your activation link <b><a>localhost:8181/auth/activationLink/123</a></b>' // html body
          html:   '<body>'+
                    '<table border="15px" style="width: 500px; border-color: #2070DD" align="center">'+
                      '<tr>'+
                        '<td style="text-align: center;"><br> '+
                          '<img src="http://elearning-net.ligarian.com/Content/images/SkyAce-logo-x2.png"></br>'+
                          '<h2><font face="comic sans" color="black"Reset Password - Sky Ace</font></h2></br>'+
                          '<p style="font-family: comic sans; font-size: 18px;">Just click to reset your password.</p></br>'+
                          '<h3><a style="background-color: #2196F3; color: white; padding: 15px 25px; font-family: comic sans; font-size: 18px; cursor: pointer;" href="'+url+'?code=' + resetPasswordToken + '">Reset Password</a></h3></br></br></br></br></br>'+
                          '<p style="font-family: comic sans; font-size: 16px;">Copyright &copy; eLearning-NET Singapore 2017</p></br>'+
                        '</td>'+
                    '</table>'+
                  '</body>'
      };

      console.log(mailOptions2);
      try {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        //nodemailer.createTestAccount((err, account) => {

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: strapi.api.email.config.smtp.service.host,
            port: strapi.api.email.config.smtp.service.port,
            secure: strapi.api.email.config.smtp.service.secure, 
            auth: {
              user: strapi.api.email.config.smtp.service.user,
              pass: strapi.api.email.config.smtp.service.pass
            },
            tls: { 
                rejectUnauthorized: false 
            }
        });

        // send mail with defined transport object
        transporter.sendMail(mailOptions2, (error, info) => {
            if (error) {
                console.log("ada error ::: " + error);
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });
        this.status = 200;
        this.body = {};
        
      } catch (err) {
        this.status = err && err.status || 500;
        this.body = err;
        console.log(err);
      }

      // try {
      //   yield strapi.api.email.services.email.send({
      //     to: user.email,
      //     subject: 'Reset password',
      //     text: url + '?code=' + resetPasswordToken,
      //     html: url + '?code=' + resetPasswordToken
      //   });
      //   this.status = 200;
      //   this.body = {};
      // } catch (err) {
      //   this.status = 500;
      //   this.body = {
      //     message: 'Error sending the email'
      //   };
      // }
  },

  forgotPasswordOld: function * () {
    const email = this.request.body.email;
    const url = this.request.body.url || strapi.config.url;
    let user;

    try {
      // Find the user user thanks to his email.
      user = yield User.findOne({email: email});

      // User not found.
      if (!user) {
        this.status = 400;
        return this.body = {
          message: 'This email does not exist.'
        };
      }
    } catch (err) {
      this.status = 500;
      return this.body = {
        message: err.message
      };
    }

    // Generate random token.
    const resetPasswordToken = crypto.randomBytes(64).toString('hex');

    // Set the property code of the local passport.
    user.resetPasswordToken = resetPasswordToken;

    // Update the user.
    try {
      user = yield user.save();
    } catch (err) {
      this.status = 500;
      return this.body = {
        message: err.message
      };
    }

    // Send an email to the user.
    try {
      yield strapi.api.email.services.email.send({
        to: user.email,
        subject: 'Reset password',
        text: url + '?code=' + resetPasswordToken,
        html: url + '?code=' + resetPasswordToken
      });
      this.status = 200;
      this.body = {};
    } catch (err) {
      this.status = 500;
      this.body = {
        message: 'Error sending the email'
      };
    }
  },

  /**
   * Change user password.
   */

  changePassword: function * () {
    // Init variables.
    const ctx = this;
    const params = _.assign({}, this.request.body, this.params);
    let user;
    console.log(params);

    if(!params.password && params.email && params.username){
      try {
        user = yield User.findOne({email: params.email});
        if(user.password_plain == "password"){
   
          // Set the new password (automatically crypted in the `beforeUpdate` function).
          user.password = params.passwordNew;
          user.password_plain = params.passwordNew;

          // Update the user.
          user = yield user.save();

          this.status = 200;
          return this.body = {
            jwt: strapi.api.user.services.jwt.issue(user),
            user: user
          };
        }else{
          this.status = 500;
          return this.body = {
            message: "Please Input Old Password"
          };
        }
      } catch (err) {
        this.status = 500;
        return this.body = {
          message: err.message
        };
      } 

    }
    else if (params.password && params.email && params.username) {

      try {
        user = yield User.findOne({email: params.email});

        const validPassword = user.validatePassword(params.password);

        if (!validPassword) {
          ctx.status = 403;
          return ctx.body = {
            message: 'Identifier or password invalid.'
          };
        } else {
          // Set the new password (automatically crypted in the `beforeUpdate` function).
          user.password = params.passwordNew;
          user.password_plain = params.passwordNew;

          // Update the user.
          user = yield user.save();

          this.status = 200;
          return this.body = {
            jwt: strapi.api.user.services.jwt.issue(user),
            user: user
          };
        }
 
      } catch (err) {
        this.status = 500;
        return this.body = {
          message: err.message
        };
      } 
    } else {
      this.status = 400;
      return this.body = {
        status: 'error',
        message: 'Incorrect params provided.'
      };
    }
  },

  changePasswordOld: function * () {
    // Init variables.
    const params = _.assign({}, this.request.body, this.params);
    let user;

    if (params.password && params.passwordConfirmation && params.password === params.passwordConfirmation && params.code) {

      try {
        user = yield User.findOne({resetPasswordToken: params.code});

        if (!user) {
          this.status = 400;
          return this.body = {
            message: 'Incorrect code provided.'
          };
        }

        // Delete the current code
        user.resetPasswordToken = null;

        // Set the new password (automatically crypted in the `beforeUpdate` function).
        user.password = params.password;
        user.password_plain = params.password;

        // Update the user.
        user = yield user.save();

        this.status = 200;
        return this.body = {
          jwt: strapi.api.user.services.jwt.issue(user),
          user: user
        };
      } catch (err) {
        this.status = 500;
        return this.body = {
          message: err.message
        };
      }
    } else if (params.password && params.passwordConfirmation && params.password !== params.passwordConfirmation) {
      this.status = 400;
      return this.body = {
        message: 'Passwords not matching.'
      };
    } else {
      this.status = 400;
      return this.body = {
        status: 'error',
        message: 'Incorrect params provided.'
      };
    }
  }
};
