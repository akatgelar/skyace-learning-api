'use strict';

const model = 'user';

/**
 * A set of functions called "actions" for `user`
 */

module.exports = {

  /**
   * Get user entries.
   *
   * @return {Object|Array}
   */

  find: function * () {
    this.model = model;
    try {
      const entry = yield strapi.hooks.blueprints.find(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Get a specific user.
   *
   * @return {Object|Array}
   */

  findOne: function * () {
    this.model = model;
    try {
      const entry = yield strapi.hooks.blueprints.findOne(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Create a user entry.
   *
   * @return {Object}
   */

  create: function * () {
    this.model = model;
    try {
      const entry = yield strapi.hooks.blueprints.create(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Update a user entry.
   *
   * @return {Object}
   */

  update: function * () {
    this.model = model;
    try {
      const entry = yield strapi.hooks.blueprints.update(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Destroy a user entry.
   *
   * @return {Object}
   */

  destroy: function * () {
    this.model = model;
    try {
      const entry = yield strapi.hooks.blueprints.destroy(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Add an entry to a specific user.
   *
   * @return {Object}
   */

  add: function * () {
    this.model = model;
    try {
      const entry = yield strapi.hooks.blueprints.add(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Remove a specific entry from a specific user.
   *
   * @return {Object}
   */

  remove: function * () {
    this.model = model;
    try {
      const entry = yield strapi.hooks.blueprints.remove(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },



  findCustom: function * () {
    this.model = model; 
    let data=[];
    let results; 
    let i=0;
    let count=0;

    try {

      /* get data provinsi*/
      // let entries = yield strapi.hooks.blueprints.find(this);
      // this.body = entries;
      // console.log("dbg \n : "+JSON.stringify(dbg));
      results = yield User.find({  
        select: [
          'username',
          'email', 
          'password_plain', 
          'roles', 
          'active_status', 
          'first_name',
          'last_name',
          'phone_code',
          'phone_number',
          'profile_picture',
          'country',
          'nationality',
          'city',
          'rece',
          'gender',
          'religion',
          'academic_level',
          'language',
          'language_user',
          'other_name',
          'birth_date',
          'passport',
          'address',
          'postal_code',
          'payment_method',
          'bank_name',
          'account_name',
          'account_number',
          'createdAt',
        ],
        where: Object.assign(
          { 
            //publish: true,
          },
          strapi.hooks.blueprints.actionUtil.parseCriteria(this),
          {}
        ),
        limit: strapi.hooks.blueprints.actionUtil.parseLimit(this),
        skip: strapi.hooks.blueprints.actionUtil.parseSkip(this),
        sort: strapi.hooks.blueprints.actionUtil.parseSort(this)
      })
      .populate('roles')
      .populate('country')
      .populate('nationality')
      .populate('city') 
      .populate('race') 
      .populate('gender') 
      .populate('religion') 
      .populate('academic_level') 
      .populate('language') 
      .populate('language_user') 
      .populate('payment_method');
      /* count data rt*/
      count = results.length;
      console.log("Count "+count);
      for(i=0;i<count;i++) 
      { 
        // //console.log("i="+i);
        // let temp_total = 0;
        // temp_total = yield Rt.count({
        //   where: Object.assign(
        //   {},
        //   strapi.hooks.blueprints.find(this),
        //   {
        //     // 'id_rw' : results[i]['id_rw'],
        //     or: [
        //       {
        //         'rw': results[i]['id']
        //       }
        //     ]
        //   })
        // });  
        data[i] = {  
          "id": results[i]['id'],    
          "username": results[i]['username'],    
          "email": results[i]['email'],     
          "password_plain": results[i]['password_plain'],     
          "roles": results[i]['roles'],     
          "active_status": results[i]['active_status'],    
          "first_name": results[i]['first_name'],
          "last_name": results[i]['last_name'],
          "phone_code": results[i]['phone_code'],
          "phone_number": results[i]['phone_number'],
          "profile_picture": results[i]['profile_picture'],
          "country": results[i]['country'],
          "city": results[i]['city'], 
          "rece": results[i]['rece'], 
          "gender": results[i]['gender'], 
          "religion": results[i]['religion'],  
          "nationality": results[i]['nationality'],  
          "academic_level": results[i]['academic_level'], 
          "language": results[i]['language'], 
          "language_user": results[i]['language_user'], 
          "other_name": results[i]['other_name'], 
          "birth_date": results[i]['birth_date'], 
          "passport": results[i]['passport'], 
          "address": results[i]['address'], 
          "postal_code": results[i]['postal_code'], 
          "payment_method": results[i]['payment_method'], 
          "bank_name": results[i]['bank_name'], 
          "account_name": results[i]['account_name'], 
          "account_number": results[i]['account_number'], 
          "createdAt":results[i]['createdAt'],
        };
      }   
      
      this.body = data;

    } catch (err) {
      this.body = err;
    }
  },

  findOneCustom: function * () {
    this.model = model; 
    let id = this.params.id; 
 
    let data=[];
    let results; 
    let i=0;
    let count=0;

    try {

      /* get data provinsi*/
      // let entries = yield strapi.hooks.blueprints.find(this);
      // this.body = entries;
      // console.log("dbg \n : "+JSON.stringify(dbg));
      results = yield User.find({  
        select: [
          'username',
          'email', 
          'password_plain', 
          'roles', 
          'active_status', 
          'first_name',
          'last_name',
          'phone_code',
          'phone_number',
          'profile_picture',
          'country',
          'nationality',
          'city',
          'rece',
          'gender',
          'religion',
          'academic_level',
          'language',
          'language_user',
          'other_name',
          'birth_date',
          'passport',
          'address',
          'postal_code',
          'payment_method',
          'bank_name',
          'account_name',
          'account_number',
          'createdAt',
        ],
        where: Object.assign(
          { 
            id: id,
          },
          strapi.hooks.blueprints.actionUtil.parseCriteria(this),
          {}
        ),
        limit: strapi.hooks.blueprints.actionUtil.parseLimit(this),
        skip: strapi.hooks.blueprints.actionUtil.parseSkip(this),
        sort: strapi.hooks.blueprints.actionUtil.parseSort(this)
      })
      .populate('roles')
      .populate('country')
      .populate('nationality')
      .populate('city') 
      .populate('race') 
      .populate('gender') 
      .populate('religion') 
      .populate('academic_level') 
      .populate('language') 
      .populate('language_user') 
      .populate('payment_method');
      /* count data rt*/
      count = results.length;
      console.log("Count "+count);
      for(i=0;i<count;i++) 
      { 
        // //console.log("i="+i);
        // let temp_total = 0;
        // temp_total = yield Rt.count({
        //   where: Object.assign(
        //   {},
        //   strapi.hooks.blueprints.find(this),
        //   {
        //     // 'id_rw' : results[i]['id_rw'],
        //     or: [
        //       {
        //         'rw': results[i]['id']
        //       }
        //     ]
        //   })
        // });  
        data[i] = {  
          "id": results[i]['id'],    
          "username": results[i]['username'],    
          "email": results[i]['email'],     
          "password_plain": results[i]['password_plain'],     
          "roles": results[i]['roles'],     
          "active_status": results[i]['active_status'],    
          "first_name": results[i]['first_name'],
          "last_name": results[i]['last_name'],
          "phone_code": results[i]['phone_code'],
          "phone_number": results[i]['phone_number'],
          "profile_picture": results[i]['profile_picture'],
          "country": results[i]['country'],
          "city": results[i]['city'], 
          "rece": results[i]['rece'], 
          "gender": results[i]['gender'], 
          "religion": results[i]['religion'],  
          "nationality": results[i]['nationality'],  
          "academic_level": results[i]['academic_level'], 
          "language": results[i]['language'], 
          "language_user": results[i]['language_user'], 
          "other_name": results[i]['other_name'], 
          "birth_date": results[i]['birth_date'], 
          "passport": results[i]['passport'], 
          "address": results[i]['address'], 
          "postal_code": results[i]['postal_code'], 
          "payment_method": results[i]['payment_method'], 
          "bank_name": results[i]['bank_name'], 
          "account_name": results[i]['account_name'], 
          "account_number": results[i]['account_number'], 
          "createdAt":results[i]['createdAt'],
        };
      }   
      
      this.body = data[0];

    } catch (err) {
      this.body = err;
    } 
  },


  //Roles
  findRoles: function * () {
    this.model = 'role';
    // this.model = model; 
    // let json_where = strapi.hooks.blueprints.actionUtil.parseCriteria(this);
    // console.log(json_where);
    try {
      var entry =  Role.find()
      // if(json_where){
      //   entry.where('json_where')  
      // }
      entry.populate('users')
      entry = yield entry; 
      this.body = entry;
    } catch (err) {
      console.log(err);
      this.body = err;
    }
  },

  updateRoles: function * () {
    try {
      user = yield User.findOne({id: this.params });
      user.roles.add(_.find(roles, {name: 'user'}));
      user = yield user.save();
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  //Routes
  findRoutes: function * () {
    this.model = 'route';
    try {
      const entry = yield Route.find({});
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  //Update Route 
  updateRoutes: function * () {
    this.model = 'route';
    try {
      const entry = yield strapi.hooks.blueprints.update(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  }
  
};
