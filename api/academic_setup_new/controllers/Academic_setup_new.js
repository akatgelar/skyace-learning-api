'use strict';

const model = 'academic_setup_new';

/**
 * A set of functions called "actions" for `academic_setup_new`
 */

module.exports = {

  /**
   * Get academic_setup_new entries.
   *
   * @return {Object|Array}
   */

  find: function * () {
    this.model = model; 
    let data=[];  
    let count=0;
    let json_where = strapi.hooks.blueprints.actionUtil.parseCriteria(this);
    let json_limit = strapi.hooks.blueprints.actionUtil.parseLimit(this);
    let json_skip = strapi.hooks.blueprints.actionUtil.parseSkip(this);
    let json_sort = strapi.hooks.blueprints.actionUtil.parseSort(this);
    console.log("where ");
    console.log(json_where);
    console.log("limit "+json_limit);
    console.log("skip "+json_skip);
    console.log("sort "+json_sort);

    try {
      var results = Academic_setup_new.find()

      if(json_where['country']){  
        results.where({'country':json_where['country']})
      }
      if(json_where['academic_level']){  
        results.where({'academic_level':json_where['academic_level']})
      } 

      results.populate('country')
      results.populate('academic_level') 
      results.populate('academic_grade') 
      results.populate('course') 
        
      // if(json_where['academic_grade']){    
      //   results.populate('academic_grade', {id: json_where['academic_grade']})
      // }else{
      //   results.populate('academic_grade');
      // }
 
      // if(json_where['course']){    
      //   results.populate('course', {id: json_where['course']})
      // }else{
      //   results.populate('course');
      // }
 
      if(json_limit){
        results.limit(json_limit)  
      }
      if(json_skip){
        results.skip(json_skip) 
      }
      if(json_sort){ 
        results.sort(json_sort)  
      }

      results = yield results; 
      // console.log(results);
  
      count = results.length;
      for(var i=0;i<count;i++) 
      {  
        var status_academic_grade = false;
        var status_grade = false;

        for(var j=0;j<results[i]['academic_grade'].length;j++) 
        {  
          // console.log("academic_grade");
          // console.log(results[i]['academic_grade'][j]);
          if(results[i]['academic_grade'][j]['id'] == json_where['academic_grade']){
            console.log("academic_grade true");
            status_academic_grade = true;
          }
        }
        for(var k=0;k<results[i]['course'].length;k++) 
        {  
          // console.log("course");
          // console.log(results[i]['course'][k]);
          if(results[i]['course'][k]['id'] == json_where['course']){
            console.log("course true");
            status_grade = true;
          }
        }


        var temp = {  
          "id": results[i]['id'],    
          "country": results[i]['country'],    
          "academic_level": results[i]['academic_level'],     
          "academic_grade": results[i]['academic_grade'],    
          "course": results[i]['course'],
          "no_session": results[i]['no_session'],
          "no_hours_per_session": results[i]['no_hours_per_session'], 
        };

        if(json_where['academic_grade'] && json_where['course'])
        {
          console.log("dua where");
          if(status_academic_grade == true && status_grade == true)
          {
            console.log(" dua true ");
            data.push(temp);
          }
        }
        else if(json_where['academic_grade'] && !json_where['course']){
          console.log("academic_grade where");
          if(status_academic_grade == true && status_grade == false)
          {
            console.log(" academic_grade true");
            data.push(temp);
          }
        }
        else if(!json_where['academic_grade'] && json_where['course']){
          console.log("course where");
          if(status_academic_grade == false && status_grade == true)
          {
            console.log(" course true");
            data.push(temp);
          }
        }
        else if(!json_where['academic_grade'] && !json_where['course']){
          console.log("else");
          data.push(temp);
        }
        
      } 

      // console.log(data);

      this.body = data;
    } catch (err) {
      this.body = err;
      console.log(err);
    }
  },

  /**
   * Get a specific academic_setup_new.
   *
   * @return {Object|Array}
   */

  findOne: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.findOne(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Create a academic_setup_new entry.
   *
   * @return {Object}
   */

  create: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.create(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Update a academic_setup_new entry.
   *
   * @return {Object}
   */

  update: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.update(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Destroy a academic_setup_new entry.
   *
   * @return {Object}
   */

  destroy: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.destroy(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Add an entry to a specific academic_setup_new.
   *
   * @return {Object}
   */

  add: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.add(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Remove a specific entry from a specific academic_setup_new.
   *
   * @return {Object}
   */

  remove: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.remove(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  }
};
