'use strict';

const model = 'tuition_activity';

/**
 * A set of functions called "actions" for `tuition_activity`
 */

const nodemailer = require('nodemailer'); 

module.exports = {

  /**
   * Get tuition_activity entries.
   *
   * @return {Object|Array}
   */

  find: function * () {
    this.model = model;
    try {
      let entries = yield strapi.hooks.blueprints.find(this);
      this.body = entries;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Get a specific tuition_activity.
   *
   * @return {Object|Array}
   */

  findOne: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.findOne(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Create a tuition_activity entry.
   *
   * @return {Object}
   */

  create: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.create(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Update a tuition_activity entry.
   *
   * @return {Object}
   */

  update: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.update(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Destroy a tuition_activity entry.
   *
   * @return {Object}
   */

  destroy: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.destroy(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Add an entry to a specific tuition_activity.
   *
   * @return {Object}
   */

  add: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.add(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Remove a specific entry from a specific tuition_activity.
   *
   * @return {Object}
   */

  remove: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.remove(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  createCustom: function * () {
    this.model = model; 
    let data = this.request.body;
    try {
      
      var str_periode_start = new Date(data.periode_start);
      var str_periode_end = new Date(data.periode_end);
      str_periode_start.setHours(str_periode_start.getHours() + 7);
      str_periode_end.setHours(str_periode_end.getHours() + 7);

      data.periode_start = str_periode_start;
      data.periode_end = str_periode_end;

      console.log(data.course);
      console.log(data.academic_grade); 
      var tgl = new Date();
      var code_course = '';
      var code_academic_grade = '';
      var code_tuition_activity =  '';
      var count = 0;

      tgl = tgl.toISOString();
      tgl = tgl.substring(0,10);
      tgl = tgl.split('-');
      tgl = tgl[0]+tgl[1]+tgl[2];
      count = yield Tuition_activity.count();
      count = count + 1;
      code_course = yield Course.find().where({'id':data.course});  

      if(count < 10) {
        count = "0000"+count;
      }else if(10 <= count < 100){
        count = "000"+count;
      }else if(100 <= count < 1000){
        count = "00"+count;
      }else if(1000 <= count < 10000){
        count = "0" + count;
      }else if(10000 <= count < 100000){
        count = count;
      }
      if(data.academic_grade)
      { 
        code_academic_grade = yield Academic_grade.find().where({'id':data.academic_grade});   
        // console.log(code_course);
        // console.log(code_academic_grade);
        code_tuition_activity = code_course[0]['code']+code_academic_grade[0]['code']+'-'+tgl+'-'+count;
      }
      else
      {
        code_tuition_activity = code_course[0]['code']+'-'+tgl+'-'+count; 
      }
      console.log(code_tuition_activity);
      data.code = code_tuition_activity;
      

      // console.log(data);
      let entry = yield Tuition_activity.create(data); 
      this.body = entry;
 
      // console.log("=============================================");
      // console.log(data.periode_start);
      // console.log(data.session_number);
      // console.log(data.session_required);
      // console.log(entry.id); 
      // console.log("============================================="); 
  

      // var date = "2017-10-11T17:00:00.000Z";
      // var day_hour_array =  [ 
      //   "Monday",
      //   "Friday",
      //   "Saturday"
      //   ];
      var date = data.periode_start;
      var day_hour_array = data.day_hour_array;

      var date_format  = new Date(date);  
      var date_string = date.toISOString();;
      var date_array = new Array();

      var split = date_string.split('T')
      var date_split = split[0].split('-');
      var time_split = split[1].split(':');
      var _date = date_split[0] + "-" + date_split[1] + "-" + date_split[2];
      var _time = time_split[0] + ":" + time_split[1] + ":" + "00";

      // console.log(date);
      // console.log(_date); 
      // console.log(_time); 

      var count_session = data.session_number * data.session_required;
      var count_day = day_hour_array.length;
      var loop = Math.ceil(count_session/count_day);

      // console.log(count_session);
      // console.log(count_day); 
      // console.log(loop); 
      // console.log("=======================");

      var session = 0;
      var addday = 0;
      var now = new Date(date_format);
      var temp = new Date(); 
      for (var i=0; i<loop; i++) {

        now.setDate(now.getDate() + 7);
        // temp = now;   
        // console.log("==============================");
        // console.log(i);
        // console.log(now.toDateString()); 
        // console.log("==============================");

        for(var j=0; j<count_day; j++){

          session = session + 1;

          var day1 = day_hour_array[j];
          var day2 = 0;
          if(day1 == "Sunday"){
            day2 = 0;
          }else if(day1 == "Monday"){
            day2 = 1;
          }else if(day1 == "Tuesday"){
            day2 = 2;
          }else if(day1 == "Wednesday"){
            day2 = 3;
          }else if(day1 == "Thursday"){
            day2 = 4;
          }else if(day1 == "Friday"){
            day2 = 5;
          }else if(day1 == "Saturday"){
            day2 = 6;
          }
          
          
          addday = 0;
          addday = (day2 - now.getDay()) % 7;

          // console.log("");
          // console.log(day1); 
          // console.log(now); 
          // console.log(addday);

          // var temp = new Date(); 
          temp.setDate(now.getDate() + parseInt(addday));
          var temp2 = temp.toISOString();
          
          // if(session <= count_session)
          // {
          //   console.log(temp); 
          // }

          
          var split = temp2.split('T')
          var date_split = split[0].split('-'); 
          var temp_date = date_split[0] + "-" + date_split[1] + "-" + date_split[2]; 
          var temp_time = _time;
          var temp_date_time = new Date(temp_date + " " + temp_time);
          temp_date_time.setHours(temp_date_time.getHours() + 7);

          // var temp_date_time = temp2;
          
          // console.log(temp_date + " " + temp_time);
          // date_array.push(temp_date + " " + temp_time);
          
          var array = { 
            "tuition_activity" : entry.id,
            "session" : session,
            "datetime" : temp_date_time,
            "doc_assigment" : ".doc",
            "video_recording" : ".mkv",
            "chat_log" : ".txt",
            "put_on_hold" : "true",
            "status_active" : "Soon"
          };

          // console.log(array);
          if(session <= count_session)
          {
            let res = yield Tuition_activity_session.create(array);
          }
          
          // console.log(res);

        } 
      }

      // console.log(date_array);

      // entry.id

      // const user = yield strapi.api.user.services.grant.connect(provider, access_token);
      const tempdata = yield strapi.api.tuition_activity.controllers.tuition_activity.findOneCustomParams(entry.id);
      var sekarang = Date();
      var manager = Role.find()
      manager.where({'id':'5a040a9ffd8c37d41a61117a'})  
      manager.populate('users') 
      manager = yield manager;  
      
      var manager2 = "";
      for(var c=0; c<=manager.length; c++){
        manager2 = manager2 + manager[0]['users'][c]['email'] + ',';
      }
      console.log(manager2)
      // console.log(tempdata); 

      let mailOptions2 = ""; 
      mailOptions2 = {
        from: 'SkyAce', // sender address
        headers : 'SkyAce',
        // to: 'akatgelar@gmail.com', // list of receivers
        subject: 'SkyAce - New Booking #' + tempdata.code, // Subject line
        to: tempdata.student.email, // list of receivers
        cc: manager2,
        // text: 'Hello world?', // plain text body
        // html: 'This is your activation link <b><a>localhost:8181/auth/activationLink/123</a></b>' // html body
        html:   '<br>' +
                'Dear '+ tempdata.student.first_name + ' ' + tempdata.student.last_name + ',' +
                '<br><br>'+
                'Here are your course details,' +
                '<br><br>' +
                '<table border="1">'+
                  '<tr>'+
                    '<td><b>Registration Date</b></td>'+
                    '<td>'+sekarang.toString()+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Reference ID</b></td>'+
                    '<td>'+tempdata.code+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Subject</b></td>'+
                    '<td>'+tempdata.course.course_name+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Academic Level</b></td>'+
                    '<td>'+tempdata.academic_level.academic_level_name+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Academic Grade</b></td>'+
                    '<td>'+tempdata.academic_grade.academic_grade+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Tutor</b></td>'+
                    '<td>'+tempdata.teacher.first_name+' '+tempdata.teacher.last_name+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>No. of lessons</b></td>'+
                    '<td>'+tempdata.session_number+' x 1.5 hrs each'+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Requrring</b></td>'+
                    '<td>'+tempdata.session_required+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Price payable per month</b></td>'+
                    '<td>'+'SGD '+tempdata.fee_per_hour+' per hour x '+ Number(tempdata.session_number*1.5) + ' hours = SGD '+ tempdata.fee_total +'</td>'+
                  '</tr>'+
                '</table>'+
                '<br><br>' +
                'Do review the above and let us know if there are any inaccuracies.' +
                '<br>' +
                'Should you need to clarify futher, please email us at <b>studentcare@skyace.com</b>.' +
                '<br>'+
                'You can also visit our website for futher information.'+
                '<br><br>' +
                'From Team SkyAce'+
                '<br>'+
                '<b><a href="http://skyace.ligarian.com/">skyace.com</a></b>' +
                '<br><br>'
      };

      console.log(mailOptions2);
      try {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        //nodemailer.createTestAccount((err, account) => {

            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: strapi.api.email.config.smtp.service.host,
                port: strapi.api.email.config.smtp.service.port,
                secure: strapi.api.email.config.smtp.service.secure, 
                auth: {
                  user: strapi.api.email.config.smtp.service.user,
                  pass: strapi.api.email.config.smtp.service.pass
                },
                tls: { 
                    rejectUnauthorized: false 
                }
            });
  
            // send mail with defined transport object
            transporter.sendMail(mailOptions2, (error, info) => {
                if (error) {
                    console.log("ada error ::: " + error);
                    return console.log(error);
                }
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
                // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
            });
        //});
      } catch (err) {
        this.status = err && err.status || 500;
        this.body = err;
        console.log(err);
      } 
      

      // email for tutor
      let mailOptions3 = ""; 
      mailOptions3 = {
        from: 'SkyAce', // sender address
        headers : 'SkyAce',
        // to: 'akatgelar@gmail.com', // list of receivers
        subject: 'SkyAce - New Booking #' + tempdata.code, // Subject line
        to: tempdata.teacher.email, // list of receivers
        cc: manager2,
        // text: 'Hello world?', // plain text body
        // html: 'This is your activation link <b><a>localhost:8181/auth/activationLink/123</a></b>' // html body
        html:   '<br>' +
                'Dear '+ tempdata.teacher.first_name + ' ' + tempdata.teacher.last_name + ',' +
                '<br><br>'+
                'You have a new student! Here are the course detail,' +
                '<br><br>' +
                '<table border="1">'+
                  '<tr>'+
                    '<td><b>Registration Date</b></td>'+
                    '<td>'+sekarang.toString()+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Reference ID</b></td>'+
                    '<td>'+tempdata.code+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Subject</b></td>'+
                    '<td>'+tempdata.course.course_name+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Academic Level</b></td>'+
                    '<td>'+tempdata.academic_level.academic_level_name+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Academic Grade</b></td>'+
                    '<td>'+tempdata.academic_grade.academic_grade+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Student</b></td>'+
                    '<td>'+tempdata.student.first_name+' '+tempdata.student.last_name+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>No. of lessons</b></td>'+
                    '<td>'+tempdata.session_number+' x 1.5 hrs each'+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Requrring</b></td>'+
                    '<td>'+tempdata.session_required+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td><b>Price payable per month</b></td>'+
                    '<td>'+'SGD '+tempdata.fee_per_hour+' per hour x '+ Number(tempdata.session_number*1.5) + ' hours = SGD '+ tempdata.fee_total +'</td>'+
                  '</tr>'+
                '</table>'+
                '<br><br>' +
                'Do review the above and let us know if there are any inaccuracies.' +
                '<br>' +
                'Should you need to clarify futher, please email us at <b>studentcare@skyace.com</b>.' +
                '<br>'+
                'You can also visit our website for futher information.'+
                '<br><br>' +
                'From Team SkyAce'+
                '<br>'+
                '<b><a href="http://skyace.ligarian.com/">skyace.com</a></b>' +
                '<br><br>'
      };

      console.log(mailOptions3);
      try {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        //nodemailer.createTestAccount((err, account) => {

            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: strapi.api.email.config.smtp.service.host,
                port: strapi.api.email.config.smtp.service.port,
                secure: strapi.api.email.config.smtp.service.secure, 
                auth: {
                  user: strapi.api.email.config.smtp.service.user,
                  pass: strapi.api.email.config.smtp.service.pass
                },
                tls: { 
                    rejectUnauthorized: false 
                }
            });
  
            // send mail with defined transport object
            transporter.sendMail(mailOptions3, (error, info) => {
                if (error) {
                    console.log("ada error ::: " + error);
                    return console.log(error);
                }
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
                // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
            });
        //});
      } catch (err) {
        this.status = err && err.status || 500;
        this.body = err;
        console.log(err);
      }
 
 
       
 
    } catch (err) {
      this.body = err; 
      console.log(err); 
    }
  },

  findCustom: function * () {
    this.model = model; 
    let data=[];  
    let id = this.params.id; 
    let json_where = strapi.hooks.blueprints.actionUtil.parseCriteria(this);
    let json_limit = strapi.hooks.blueprints.actionUtil.parseLimit(this);
    let json_skip = strapi.hooks.blueprints.actionUtil.parseSkip(this);
    let json_sort = strapi.hooks.blueprints.actionUtil.parseSort(this);
    // console.log(json_where); 
 
    try {
 
      // mulai select
      var results = Tuition_activity.find()
    
      results.where(json_where) 

      // populate kategori atas
      results.populate('tutor_teaching_subject')
      results.populate('student')
      results.populate('teacher')
      results.populate('country')
      results.populate('academic_level')
      results.populate('academic_grade')
      results.populate('course')
      results.populate('activity_status')
          

      if(json_limit){
        results.limit(json_limit)  
      }
      if(json_skip){
        result.skip(json_skip) 
      }
      if(json_sort){
        result.sort(json_sort)  
      }

      results = yield results; 
 
      // console.log("length = " + results.length);
      console.log(results);
      for(var i=0;i<results.length;i++) 
      { 
       
        var data_tutor_reference = new Object();
        var data_tutor_working_experience = new Object();
        var data_tutor_teaching_subject = new Object();
        var data_tutor_calendar = new Object();
        var data_tuition_activity_session = new Object(); 

        data_tutor_reference = yield Tutor_reference.find().where({'user':results[i]['teacher']['id']});
        data_tutor_working_experience = yield Tutor_working_experience.find().where({'user':results[i]['teacher']['id']});
        if(results[i]['tutor_teaching_subject']){ 
          data_tutor_teaching_subject = yield Tutor_teaching_subject.find().where({'id':results[i]['tutor_teaching_subject']['id']});
        }
        data_tutor_calendar = yield Tutor_calendar.find().where({'user':results[i]['teacher']['id']});
        data_tuition_activity_session = yield Tuition_activity_session.find().where({'tuition_activity':results[i]['id']});
   
        var array = {  
          "id": results[i]['id'],                                           
          "tutor_reference": data_tutor_reference[0],                 
          "tutor_working_experience": data_tutor_working_experience,                 
          "tutor_teaching_subject": data_tutor_teaching_subject[0],            
          "tutor_calendar": data_tutor_calendar[0],            
          "tuition_activity_session": data_tuition_activity_session,                        
          "student": results[i]['student'],                        
          "teacher": results[i]['teacher'],                        
          "country": results[i]['country'],           
          "academic_level": results[i]['academic_level'],         
          "academic_grade": results[i]['academic_grade'],         
          "periode_start": results[i]['periode_start'],         
          "periode_end": results[i]['periode_end'],       
          "day_hour_array": results[i]['day_hour_array'],       
          "session_number": results[i]['session_number'],       
          "session_required": results[i]['session_required'],       
          "fee_per_hour": results[i]['fee_per_hour'],       
          "fee_total": results[i]['fee_total'],       
          "activity_status": results[i]['activity_status'],       
          "paid_student": results[i]['paid_student'],        
          "paid_teacher": results[i]['paid_teacher'],        
          "createdBy": results[i]['createdBy'],        
        };
        data.push(array);
      

      } 

      this.body = data;

    } catch (err) {
      this.body = err;
      console.log(err);
    }
  },



  findOneCustom: function * () {
    this.model = model; 
    let data=[];  
    let id = this.params.id; 
    // console.log(this.params.id); 

    try {
 
      // mulai select
      var results = Tuition_activity.find()
 
      results.where({'id':id})
       
      // populate kategori atas
      results.populate('tutor_teaching_subject')
      results.populate('student')
      results.populate('teacher')
      results.populate('country')
      results.populate('academic_level')
      results.populate('academic_grade')
      results.populate('course')
      results.populate('activity_status')
         
      results = yield results; 
 
      // console.log("length = " + results.length);
      // console.log(results);
      for(var i=0;i<results.length;i++) 
      { 
       
        var temp_tutor_teaching_subject = new Array();
        var temp_tuition_activity_session = new Array(); 

        var data_tutor_reference = new Object();
        var data_tutor_working_experience = new Object();
        var data_tutor_teaching_subject = new Object();
        var data_tutor_calendar = new Object();
        var data_tuition_activity_session = new Object(); 

        data_tutor_reference = yield Tutor_reference.find().where({'user':results[i]['teacher']['id']});
        data_tutor_working_experience = yield Tutor_working_experience.find().where({'user':results[i]['teacher']['id']});
        data_tutor_teaching_subject = yield Tutor_teaching_subject.find().where({'id':results[i]['tutor_teaching_subject']['id']});
        data_tutor_calendar = yield Tutor_calendar.find().where({'user':results[i]['teacher']['id']});
        data_tuition_activity_session = yield Tuition_activity_session.find().where({'tuition_activity':results[i]['id']});
   
        var array = {  
          "id": results[i]['id'],                                           
          "code": results[i]['code'],                                           
          "tutor_reference": data_tutor_reference[0],                 
          "tutor_working_experience": data_tutor_working_experience,                 
          "tutor_teaching_subject": data_tutor_teaching_subject[0],            
          "tutor_calendar": data_tutor_calendar[0],            
          "tuition_activity_session": data_tuition_activity_session,                        
          "student": results[i]['student'],                        
          "teacher": results[i]['teacher'],                        
          "country": results[i]['country'],           
          "academic_level": results[i]['academic_level'],         
          "academic_grade": results[i]['academic_grade'],         
          "course": results[i]['course'],         
          "periode_start": results[i]['periode_start'],         
          "periode_end": results[i]['periode_end'],       
          "day_hour_array": results[i]['day_hour_array'],       
          "session_number": results[i]['session_number'],       
          "session_required": results[i]['session_required'],       
          "fee_per_hour": results[i]['fee_per_hour'],       
          "fee_total": results[i]['fee_total'],       
          "activity_status": results[i]['activity_status'],       
          "paid_student": results[i]['paid_student'],        
          "paid_teacher": results[i]['paid_teacher'],        
          "createdBy": results[i]['createdBy'],        
        };
        data.push(array);
      

      } 

      this.body = data[0];

    } catch (err) {
      this.body = err;
    }
  },


  findOneCustomParams: function * (ids) {
    this.model = model; 
    let data=[];  
    let id = ids; 
    // console.log(this.params.id); 

    try {
 
      // mulai select
      var results = Tuition_activity.find()
 
      results.where({'id':id})
       
      // populate kategori atas
      results.populate('tutor_teaching_subject')
      results.populate('student')
      results.populate('teacher')
      results.populate('country')
      results.populate('academic_level')
      results.populate('academic_grade')
      results.populate('course')
      results.populate('activity_status')
         
      results = yield results; 
 
      // console.log("length = " + results.length);
      // console.log(results);
      for(var i=0;i<results.length;i++) 
      { 
       
        var temp_tutor_teaching_subject = new Array();
        var temp_tuition_activity_session = new Array(); 

        var data_tutor_reference = new Object();
        var data_tutor_working_experience = new Object();
        var data_tutor_teaching_subject = new Object();
        var data_tutor_calendar = new Object();
        var data_tuition_activity_session = new Object(); 

        data_tutor_reference = yield Tutor_reference.find().where({'user':results[i]['teacher']['id']});
        data_tutor_working_experience = yield Tutor_working_experience.find().where({'user':results[i]['teacher']['id']});
        data_tutor_teaching_subject = yield Tutor_teaching_subject.find().where({'id':results[i]['tutor_teaching_subject']['id']});
        data_tutor_calendar = yield Tutor_calendar.find().where({'user':results[i]['teacher']['id']});
        data_tuition_activity_session = yield Tuition_activity_session.find().where({'tuition_activity':results[i]['id']});
   
        var array = {  
          "id": results[i]['id'],                                           
          "code": results[i]['code'],                                           
          "tutor_reference": data_tutor_reference[0],                 
          "tutor_working_experience": data_tutor_working_experience,                 
          "tutor_teaching_subject": data_tutor_teaching_subject[0],            
          "tutor_calendar": data_tutor_calendar[0],            
          "tuition_activity_session": data_tuition_activity_session,                        
          "student": results[i]['student'],                        
          "teacher": results[i]['teacher'],                        
          "country": results[i]['country'],           
          "academic_level": results[i]['academic_level'],         
          "academic_grade": results[i]['academic_grade'],         
          "course": results[i]['course'],         
          "periode_start": results[i]['periode_start'],         
          "periode_end": results[i]['periode_end'],       
          "day_hour_array": results[i]['day_hour_array'],       
          "session_number": results[i]['session_number'],       
          "session_required": results[i]['session_required'],       
          "fee_per_hour": results[i]['fee_per_hour'],       
          "fee_total": results[i]['fee_total'],       
          "activity_status": results[i]['activity_status'],       
          "paid_student": results[i]['paid_student'],        
          "paid_teacher": results[i]['paid_teacher'],        
          "createdBy": results[i]['createdBy'],        
        };
        data.push(array);
      

      } 

      return data[0];

    } catch (err) {
      return err;
    }
  },



  check: function * () {
    this.model = model; 
    let data = this.request.body;
    let entry = new Object();
    try {
      
      var teacher = data.teacher;
      var periode_start = data.periode_start;
      var periode_end = data.periode_end;

      // console.log(teacher);
      // console.log(periode_start); 
      // console.log(periode_start.length)
      // console.log(periode_end);
      // console.log(periode_end.length)
      // console.log("=============================");

      var stat = true;
      var err = [];
      var or = [];
      for(var i=0;i<periode_start.length;i++) 
      { 
        var temp1 = new Date(periode_start[i] + '.000Z');
        var temp2 = new Date(periode_end[i] + '.000Z');

        var results = Tutor_calendar.find()
        results.where({'user':teacher})

        // or.push({'start_date':{'<=':temp1}},{'end_date':{'>=':temp1}});
        // or.push({'start_date':{'<=':temp2}},{'end_date':{'>=':temp2}}); 
        
        // console.log(or);
        // results.where(or);

        results.where({'start_date':{'<=':temp1}})
        results.where({'end_date':{'>=':temp1}})

        // results.where({'start_date':{'<=':temp2}})
        // results.where({'end_date':{'>=':temp2}})

        results = yield results; 
        if(results.length > 0){
          for(var c=0; c<results.length;c++){ 
            stat = false;
            err.push(results[c]);
          }
        }

        // console.log(i);
        // console.log(teacher);
        // console.log(temp1);
        // console.log(temp2);
        // console.log(results);
        // console.log(stat);
        // console.log(err);

      }
      entry.status_available = stat;
      entry.block_date = err;

      console.log(entry);

      //let entry = yield strapi.hooks.blueprints.create(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
      console.log(err);
    }
  },


  finds: function * () {
    this.model = model;
    let json_where = strapi.hooks.blueprints.actionUtil.parseCriteria(this);
    let json_limit = strapi.hooks.blueprints.actionUtil.parseLimit(this);
    let json_skip = strapi.hooks.blueprints.actionUtil.parseSkip(this);
    let json_sort = strapi.hooks.blueprints.actionUtil.parseSort(this);
    console.log(json_where);

    try {


      // var found = false;
      var results = Tuition_activity.find()
      var results2 = [];
      var results3 = [];

      if(json_where['tuition_status']){
        results.where({'tuition_status':json_where['tuition_status']})  
      }
 
      // if(json_where['name']){ 
      //   var where_teacher = {}; 
      //   var where_student = {}; 

      //   where_teacher['or'] = [{"first_name":{"contains":json_where['name']},"last_name":{"contains":json_where['name']}}];
      //   where_student['or'] = [{"first_name":{"contains":json_where['name']},"last_name":{"contains":json_where['name']}}];
        
      //   results.populate('teacher', {where: where_teacher}) 
      //   results.populate('student', {where: where_student}) 
      // }
      // else{
      //   results.populate('student');
      //   results.populate('teacher');
      // }
   
      if(json_limit){
        results.limit(json_limit)  
      }
      if(json_skip){
        results.skip(json_skip) 
      }
      if(json_sort){
        results.sort(json_sort)  
      }

      results.populate('student');
      results.populate('teacher');
      results.populate('country');
      results.populate('academic_level');
      results.populate('academic_grade');
      results.populate('course');


      results2 = yield results; 
      var stat = false;
      for(var i=0;i<results2.length;i++) 
      {
        if((results2[i]['teacher']['first_name'].toLowerCase().indexOf(json_where['name'].toLowerCase()) !== -1)||(results2[i]['teacher']['last_name'].toLowerCase().indexOf(json_where['name'].toLowerCase()) !== -1)){
          console.log("ketemu di teacher" + i);
          stat = true;
        }
        if((results2[i]['student']['first_name'].toLowerCase().indexOf(json_where['name'].toLowerCase()) !== -1)||(results2[i]['student']['last_name'].toLowerCase().indexOf(json_where['name'].toLowerCase()) !== -1)){
          console.log("ketemu di student" + i);
          stat = true;
        } 
        if(stat == true){
          results3.push(results2[i]);
        }
        stat=false;
      }
      // results3 = yield results;
      // let entries = yield strapi.hooks.blueprints.find(this);
      this.body = results3;
    } catch (err) {
      this.body = err;
      console.log(err);
    }
  }

};