'use strict';

const model = 'user_rate';

/**
 * A set of functions called "actions" for `user_rate`
 */

module.exports = {

  /**
   * Get user_rate entries.
   *
   * @return {Object|Array}
   */

  find: function * () {
    this.model = model;
    try {
      let entries = yield strapi.hooks.blueprints.find(this);
      this.body = entries;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Get a specific user_rate.
   *
   * @return {Object|Array}
   */

  findOne: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.findOne(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Create a user_rate entry.
   *
   * @return {Object}
   */

  create: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.create(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Update a user_rate entry.
   *
   * @return {Object}
   */

  update: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.update(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Destroy a user_rate entry.
   *
   * @return {Object}
   */

  destroy: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.destroy(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Add an entry to a specific user_rate.
   *
   * @return {Object}
   */

  add: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.add(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Remove a specific entry from a specific user_rate.
   *
   * @return {Object}
   */

  remove: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.remove(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  count: function * () {
    this.model = model; 
    let data=[];  
    let json_where = strapi.hooks.blueprints.actionUtil.parseCriteria(this); 
    console.log("json where ");
    console.log(json_where); 

    try {

      var where = {};  
      if(json_where['sender']){   
        where['sender'] = json_where['sender'];
      } 
      if(json_where['receiver']){  
        where['receiver'] = json_where['receiver'];
      } 
      // console.log("where");
      // console.log(where);


      var results = User_rate.find()  
      results.where(where) 
      results = yield results;  
      // console.log("result");
      // console.log(results);


      var count = 0;
      count = results.length;
      // console.log("count");
      // console.log(results.length);
      

      // console.log("num_star");
      var total = 0;
      for(var i=0;i<results.length;i++) 
      {  
        // console.log(results[i]['num_star']);
        total = total + Number(results[i]['num_star']);
      } 
      // console.log("total");
      // console.log(total);


      var avg = 0;
      avg = (total / count).toFixed(); 
      if(isNaN(avg)){
        avg = 0;
      }
      // console.log("avg");
      // console.log(avg); 
      
      var sender = [];
      if(json_where['sender']){
        sender = yield User.find().where({'id':json_where['sender']});
      }

      var receiver = [];
      if(json_where['receiver']){
        receiver = yield User.find().where({'id':json_where['receiver']});
      }

      var array = {
        "sender" : sender[0],
        "receiver" : receiver[0],
        "total" : total,
        "count" : count,
        "avg" : avg,
      };
      // console.log("array");
      // console.log(array);

      this.body = array;

    } catch (err) {
      this.body = err;
    }
  },
};
