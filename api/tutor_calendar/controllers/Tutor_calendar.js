'use strict';

const model = 'tutor_calendar';

/**
 * A set of functions called "actions" for `tutor_calendar`
 */

module.exports = {

  /**
   * Get tutor_calendar entries.
   *
   * @return {Object|Array}
   */

  find: function * () {
    this.model = model;
    try {
      let entries = yield strapi.hooks.blueprints.find(this);
      this.body = entries;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Get a specific tutor_calendar.
   *
   * @return {Object|Array}
   */

  findOne: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.findOne(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Create a tutor_calendar entry.
   *
   * @return {Object}
   */

  create: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.create(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },



  /**
   * Update a tutor_calendar entry.
   *
   * @return {Object}
   */

  update: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.update(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Destroy a tutor_calendar entry.
   *
   * @return {Object}
   */

  destroy: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.destroy(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Add an entry to a specific tutor_calendar.
   *
   * @return {Object}
   */

  add: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.add(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Remove a specific entry from a specific tutor_calendar.
   *
   * @return {Object}
   */

  remove: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.remove(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },


  createCustom: function * () {
    this.model = model;
    let data = this.request.body;
    let body = [];

    // console.log(data.user);
    // console.log(data.start_date);
    // console.log(data.end_date);
    // console.log(data.status);
    // console.log("==============================");
    try {

      var year = new Date().getFullYear();

      var start_date = new Date(data.start_date);
      var end_date = new Date(year+"-12-31");

      var start_date2 = new Date(start_date.toISOString().substring(0, 10));
      var end_date2 = new Date(end_date.toISOString().substring(0, 10));

      var start_time = data.start_date.substring(11,24);
      var end_time = data.end_date.substring(11,24);

      var weekDiff = Math.floor((end_date2 - start_date2 + 1) / (1000 * 60 * 60 * 24) / 7) + 1;

      // console.log(data.start_date);
      // console.log(data.end_date);
      // console.log(start_date2);
      // console.log(end_date2);
      console.log(start_time);
      console.log(end_time);
      // console.log(weekDiff);
      // console.log();

      var now = new Date(start_date2);

      for (var i=0; i<weekDiff; i++) {

          // console.log("==============================");
          // console.log(i);
          // console.log(now.toISOString().substring(0, 10));  

          var new_start = new Date(now.toISOString().substring(0, 10) + "T" + start_time);
          var new_end = new Date(now.toISOString().substring(0, 10) + "T" + end_time);
          var array = { 
              "user" : data.user,
              "start_date" : new_start,
              "end_date" : new_end,
              "status" : data.status
          };   
          
          // console.log(array)
          let res = yield Tutor_calendar.create(array);
          // console.log(res);
          body.push(res);
 
          now.setDate(now.getDate() + 7); 
   
      }

      this.body = body;


    } catch (err) {
      this.body = err;
    }


  }


};
