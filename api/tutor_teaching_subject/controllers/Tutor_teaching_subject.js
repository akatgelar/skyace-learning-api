'use strict';

const model = 'tutor_teaching_subject';

/**
 * A set of functions called "actions" for `tutor_teaching_subject`
 */

module.exports = {

  /**
   * Get tutor_teaching_subject entries.
   *
   * @return {Object|Array}
   */

  find: function * () {
    this.model = model;
    try {
      let entries = yield strapi.hooks.blueprints.find(this);
      this.body = entries;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Get a specific tutor_teaching_subject.
   *
   * @return {Object|Array}
   */

  findOne: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.findOne(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Create a tutor_teaching_subject entry.
   *
   * @return {Object}
   */

  create: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.create(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Update a tutor_teaching_subject entry.
   *
   * @return {Object}
   */

  update: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.update(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Destroy a tutor_teaching_subject entry.
   *
   * @return {Object}
   */

  destroy: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.destroy(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Add an entry to a specific tutor_teaching_subject.
   *
   * @return {Object}
   */

  add: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.add(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  /**
   * Remove a specific entry from a specific tutor_teaching_subject.
   *
   * @return {Object}
   */

  remove: function * () {
    this.model = model;
    try {
      let entry = yield strapi.hooks.blueprints.remove(this);
      this.body = entry;
    } catch (err) {
      this.body = err;
    }
  },

  findCustom: function * () {
    this.model = model; 
    let data=[];  
    let data2=[];  
    let json_where = strapi.hooks.blueprints.actionUtil.parseCriteria(this);
    let json_limit = strapi.hooks.blueprints.actionUtil.parseLimit(this);
    let json_skip = strapi.hooks.blueprints.actionUtil.parseSkip(this);
    let json_sort = strapi.hooks.blueprints.actionUtil.parseSort(this);
    console.log("where ");
    console.log(json_where);
    console.log("limit "+json_limit);
    console.log("skip "+json_skip);
    console.log("sort "+json_sort);

    try {

       
      // mulai select
      var results = Tutor_teaching_subject.find()
      
      // where kategori atas
      if(json_where['country']){  
        results.where({'country':json_where['country']})
      }
      if(json_where['academic_level']){  
        results.where({'academic_level':json_where['academic_level']})
      }
      if(json_where['academic_grade']){  
        results.where({'academic_grade':json_where['academic_grade']})
      }
      if(json_where['course']){  
        results.where({'course':json_where['course']})
      } 
      if(json_where['price']){   
        results.where({'fee_per_hour':json_where['price']})
      }
      if(json_where['user']){   
        results.where({'user':json_where['user']})
      } 
 
      // populate kategori atas
      results.populate('country')
      results.populate('academic_level')
      results.populate('academic_grade')
      results.populate('course')
      
      // array kategori samping
      var where_user = {}; 
      var or = [];

      // where kategori samping
      if(json_where['language']){   
        where_user['language'] = json_where['language'];
      } 
      if(json_where['gender']){  
        where_user['gender'] = json_where['gender'];
      } 
      if(json_where['nationality']){  
        where_user['country']= json_where['nationality'];
      }
      if(json_where['degree']){  
        where_user['academic_level'] = json_where['degree'];
      }
      if(json_where['race']){  
        where_user['race'] = json_where['race'];
      }

      var user_count = 0;
      if(json_where['age']){   
        // where_user['birth_date'] = json_where['age'];
        // let leg = json_where.length;
        
        var leg2 = Object.keys(json_where).length;
        // console.log("length " + leg2);
        if(leg2 == 1)
        { 
          // console.log('leg 1');
          where_user['birth_date'] = json_where['age'];
        }
        else if(leg2 > 1)
        { 
          // console.log("leg > 1");
          var ag = {};
          ag['birth_date'] = json_where['age'];

          // console.log(ag);
          or.push(ag);
          // console.log(where_user);
          user_count = user_count + 1;
        }
      }
      if(json_where['name']){   
        // console.log(json_where['name']);
        // var nn = "";
          // { or : [ { free  : true }, {   public: true  } ] } 

        var leg3 = Object.keys(json_where).length;
        // console.log("length " + leg2);
        if(leg3 == 1)
        { 
          // console.log('leg 1');
          where_user['first_name'] = {"contains":json_where['name']};
        }
        else if(leg2 > 1)
        { 
          var nn2 = {}; 
          var mm = []; 
          mm = {"first_name":{"contains":json_where['name']},"last_name":{"contains":json_where['name']}}

          or.push(mm);
          // console.log(where_user);
          user_count = user_count + 1;
        }
          
      } 
      console.log("user_count" + user_count)
      if(user_count >= 1)
      { 
        where_user['or'] = or;
      }
      console.log("where user");
      console.log(where_user);

 
 
      // populate user
      results.populate('user', {where: where_user})
      
      if(json_limit){
        results.limit(json_limit)  
      }
      if(json_skip){
        results.skip(json_skip) 
      }
      if(json_sort){ 
        results.sort(json_sort)  
      }
      

      results = yield results; 
 
      // console.log("length = " + results.length);
      for(var i=0;i<results.length;i++) 
      { 
        
        if(results[i]['user'])
        { 
 
          var temp_user = new Array();
          var temp_tutor_reference = new Array();
          var temp_tutor_working_experience = new Array();
          var temp_tutor_teaching_subject = new Array();
          var temp_tutor_calendar = new Array();
          var temp_user_rate = new Array();
          var temp_lesson = new Array();

          var data_user = new Object();
          var data_tutor_reference = new Object();
          var data_tutor_references = new Object();
          var data_tutor_working_experience = new Object();
          var data_tutor_teaching_subject =new Object();
          var data_tutor_calendar = new Object();
          var data_user_rate = new Object();
          var data_lesson = new Object();

          data_user['role'] = yield Role.find().where({'id':results[i]['user']['role']});    
          data_user['country'] = yield Country.find().where({'id':results[i]['user']['country']});   
          data_user['city']= yield City.find().where({'id':results[i]['user']['city']});    
          data_user['race'] = yield Race.find().where({'id':results[i]['user']['race']});      
          data_user['gender'] = yield Gender.find().where({'id':results[i]['user']['gender']});     
          data_user['religion'] = yield Religion.find().where({'id':results[i]['user']['religion']});     
          data_user['academic_level'] = yield Academic_level.find().where({'id':results[i]['user']['academic_level']});     
          data_user['language'] = yield Language.find().where({'id':results[i]['user']['language']});    
          data_user['payment_method'] = yield Payment_method.find().where({'id':results[i]['user']['payment_method']});  

          data_tutor_reference = yield Tutor_reference.find().where({'user':results[i]['user']['id']});   
          if(data_tutor_reference.length > 0){
            data_tutor_references['id'] = data_tutor_reference[0]['id'];
            data_tutor_references['user'] = data_tutor_reference[0]['user'];
            data_tutor_references['country'] = yield Country.find().where({'id':data_tutor_reference[0]['country']});   
            data_tutor_references['language'] = yield Language.find().where({'id':data_tutor_reference[0]['language']});     
            data_tutor_references['about_me'] = data_tutor_reference[0]['about_me'];
            data_tutor_references['profile_picture'] = data_tutor_reference[0]['profile_picture'];
            data_tutor_references['academic_level'] = yield Academic_level.find().where({'id':data_tutor_reference[0]['academic_level']}); 
            data_tutor_references['major'] = data_tutor_reference[0]['major'];
            data_tutor_references['institute'] = data_tutor_reference[0]['institute'];
            data_tutor_references['cerificate'] = data_tutor_reference[0]['cerificate'];
          }
          data_tutor_working_experience = yield Tutor_working_experience.find().where({'user':results[i]['user']['id']});    
          //data_tutor_teaching_subject = yield Tutor_teaching_subject.find().where({'user':results[i]['user']['id']});    
          data_tutor_calendar = yield Tutor_calendar.find().where({'user':results[i]['user']['id']});  

          data_user_rate = yield User_rate.find().where({'receiver':results[i]['user']['id']}); 
          var count = 0;
          var total = 0;
          var avg = 0;
          count = data_user_rate.length;  
          for(var k=0;k<data_user_rate.length;k++) {   
            total = total + Number(data_user_rate[k]['num_star']);
          }  
          avg = (total / count).toFixed();
          if(isNaN(avg)){
            avg = 0;
          }

          data_lesson['teaching_subject']  = yield Tutor_teaching_subject.count().where({'user':results[i]['user']['id']}); 
          data_lesson['tuition_activity']  = yield Tuition_activity.count().where({'user':results[i]['user']['id']}); 
          
          
           


          // console.log("data_user ");
          // console.log(data_user);
          // console.log("data_tutor_reference ");
          // console.log(data_tutor_reference);
          // console.log(data_tutor_references);
          // console.log("data_tutor_working_experience ");
          // console.log(data_tutor_working_experience);
          // console.log("data_tutor_teaching_subject ");
          // console.log(data_tutor_teaching_subject);
          // console.log("data_tutor_calendar ");
          // console.log(data_tutor_calendar); 
          // console.log("data_user_rate ");
          // console.log(data_user_rate);  
          // console.log("data_lesson"); 
          // console.log(data_lesson); 


          temp_user = {
            "id" : results[i]['user']['id'],
            "role" : data_user['role'][0],
            "active_status" : results[i]['user']['active_status'],
            "first_name" : results[i]['user']['first_name'],
            "last_name" : results[i]['user']['last_name'], 
            "email" : results[i]['user']['email'], 
            "phone_code" : results[i]['user']['phone_code'], 
            "phone_number" : results[i]['user']['phone_number'], 
            "profile_picture" : results[i]['user']['profile_picture'],  
            "country" : data_user['country'][0], 
            "city" : data_user['city'][0], 
            "race" : data_user['race'][0], 
            "gender" : data_user['gender'][0], 
            "religion" : data_user['religion'][0], 
            "academic_level" : data_user['academic_level'][0],  
            "language" : data_user['language'][0], 
            "other_name" : results[i]['user']['other_name'],  
            "birth_date" : results[i]['user']['birth_date'],  
            "passport" : results[i]['user']['passport'],  
            "address" : results[i]['user']['address'],  
            "postal_code" : results[i]['user']['postal_code'],  
            "payment_method" : data_user['payment_method'][0],  
            "bank_name" : results[i]['user']['bank_name'],  
            "account_name" : results[i]['user']['account_name'],  
            "account_number" : results[i]['user']['account_number'],  
          };
 
          // console.log("temp_user ");
          // console.log(temp_user); 
          // console.log(data_tutor_reference.length);
          // console.log(data_tutor_references.length);
          // console.log(Object.keys(data_tutor_references).length);
          // console.log(data_tutor_references);
 
          if(Object.keys(data_tutor_references).length)
          {
            temp_tutor_reference = {
              "id" : data_tutor_references['id'],
              "user" : data_tutor_references['user'],
              "country" : data_tutor_references['country'][0],
              "language" : data_tutor_references['language'][0],
              "about_me" : data_tutor_references['about_me'],  
              "profile_picture" : data_tutor_references['profile_picture'],  
              "academic_level" : data_tutor_references['academic_level'][0], 
              "major" : data_tutor_references['major'], 
              "institute" : data_tutor_references['institute'], 
              "cerificate" : data_tutor_references['certificate'], 
            };    
          }

          // console.log("temp_tutor_reference ")
          // console.log(temp_tutor_reference);

          if(data_tutor_working_experience.length > 0){

            for(var j=0;j<data_tutor_working_experience.length;j++) {

              var temp_tutor_working_experience2 = {
                "id" : data_tutor_working_experience[0]['id'],
                "user" : data_tutor_working_experience[0]['user'],
                "year" : data_tutor_working_experience[0]['year'],
                "experience" : data_tutor_working_experience[0]['experience'], 
                "note" : data_tutor_working_experience[0]['note'], 
                "verified" : data_tutor_working_experience[0]['verified'],  
              };
              temp_tutor_working_experience.push(temp_tutor_working_experience2);
            }
          }

          // console.log("temp_tutor_working_experience ");
          // console.log(temp_tutor_working_experience);

 
          temp_tutor_teaching_subject = {                 
            "id": results[i]['id'],                                            
            "user": results[i]['user']['id'],                   
            "country": results[i]['country']['id'],         
            "academic_level": results[i]['academic_level']['id'],         
            "academic_grade": results[i]['academic_grade']['id'],         
            "course": results[i]['course']['id'],         
            "fee_per_hour": results[i]['fee_per_hour'],         
            "fee_commition": results[i]['fee_commition'],         
            "fee_earn": results[i]['fee_earn'],         
            "note": results[i]['note'],  
          };

          // console.log("temp_tutor_teaching_subject ");
          // console.log(temp_tutor_teaching_subject);

          if(data_tutor_calendar.length > 0){
            temp_tutor_calendar = {
              "id" : data_tutor_calendar[0]['id'],
              "user" : data_tutor_calendar[0]['user'],
              "available_start" : data_tutor_calendar[0]['available_start'],
              "available_end" : data_tutor_calendar[0]['available_end'], 
              "blocked_start" : data_tutor_calendar[0]['blocked_start'], 
              "blocked_end" : data_tutor_calendar[0]['blocked_end'], 
              "note" : data_tutor_calendar[0]['note'], 
            };
          }

          // console.log("temp_tutor_calendar ");
          // console.log(temp_tutor_calendar); 
 
          temp_user_rate = { 
            "receiver" : results[i]['user']['id'],
            "total" : total,
            "count" : count,
            "avg" : avg,
          };         

          // console.log("temp_user_rate ");
          // console.log(temp_user_rate); 


          temp_lesson = { 
            "user" : results[i]['user']['id'],
            "teaching_subject" : data_lesson['teaching_subject'],
            "tuition_activity" : data_lesson['tuition_activity'], 
          };         

          // console.log("temp_lesson ");
          // console.log(temp_lesson); 



          var array = {  
            "id": results[i]['id'],                        
            "id_tutor_teaching_subject": results[i]['id'],                        
            "user": temp_user,              
            "tutor_reference": temp_tutor_reference,           
            "tutor_working_experience": temp_tutor_working_experience,        
            "tutor_calendar": temp_tutor_calendar,                    
            "country": results[i]['country'],         
            "academic_level": results[i]['academic_level'],         
            "academic_grade": results[i]['academic_grade'],         
            "course": results[i]['course'],         
            "user_rate": temp_user_rate,         
            "lesson": temp_lesson,         
            "fee_per_hour": results[i]['fee_per_hour'],         
            "fee_commition": results[i]['fee_commition'],         
            "fee_earn": results[i]['fee_earn'],         
            "note": results[i]['note'],  
          };
          data.push(array);
        }
      } 

      data2=data;


      //sorting
      if(json_sort)
      {
        var len = data2.length;
        var split = json_sort.split(' ');

        if(split[0] == "rate")
        {
          if(split[1] == "asc" || split[1] == "ASC")
          {
            console.log("rate asc");
            // asc
            for (var i = len-1; i>=0; i--){
             for(var j = 1; j<=i; j++){
               if(data2[j-1]['user_rate']['total']>data2[j]['user_rate']['total']){
                   var temp = data2[j-1];
                   data2[j-1] = data2[j];
                   data2[j] = temp;
                }
             }
            }
          }
          else if(split[1] == "desc" || split[1] == "DESC")
          {
            console.log("rate desc");
            // desc
            for (var i = 0; i<=len-1; i++){
             for(var j = 1; j<=i; j++){
               if(data2[j-1]['user_rate']['total']<data2[j]['user_rate']['total']){
                   var temp = data2[j-1];
                   data2[j-1] = data2[j];
                   data2[j] = temp;
                }
             }
            }
          }
        }
        else if(split[0] == "name")
        {
          if(split[1] == "asc" || split[1] == "ASC")
          {
            console.log("name asc");
            // asc
            for (var i = len-1; i>=0; i--){
             for(var j = 1; j<=i; j++){
               if(data2[j-1]['user']['first_name'].toLowerCase()>data2[j]['user']['first_name'].toLowerCase()){
                   var temp = data2[j-1];
                   data2[j-1] = data2[j];
                   data2[j] = temp;
                }
             }
            }
          }
          else if(split[1] == "desc" || split[1] == "DESC")
          {
            console.log("name desc");
            // desc
            for (var i = 0; i<=len-1; i++){
             for(var j = 1; j<=i; j++){
               if(data2[j-1]['user']['first_name'].toLowerCase()<data2[j]['user']['first_name'].toLowerCase()){
                   var temp = data2[j-1];
                   data2[j-1] = data2[j];
                   data2[j] = temp;
                }
             }
            }
          }
        }
        else if(split[0] == "tuition_activity ")
        {
          if(split[1] == "asc" || split[1] == "ASC")
          {
            console.log("tuition_activity  asc");
            // asc
            for (var i = len-1; i>=0; i--){
             for(var j = 1; j<=i; j++){
               if(data2[j-1]['lesson']['tuition_activity ']>data2[j]['lesson']['tuition_activity ']){
                   var temp = data2[j-1];
                   data2[j-1] = data2[j];
                   data2[j] = temp;
                }
             }
            }
          }
          else if(split[1] == "desc" || split[1] == "DESC")
          {
            console.log("tuition_activity  desc");
            // desc
            for (var i = 0; i<=len-1; i++){
             for(var j = 1; j<=i; j++){
               if(data2[j-1]['lesson']['tuition_activity ']<data2[j]['lesson']['tuition_activity ']){
                   var temp = data2[j-1];
                   data2[j-1] = data2[j];
                   data2[j] = temp;
                }
             }
            }
          }
        }
      }
         
      this.body = data2;

    } catch (err) {
      this.body = err;
      console.log(err);
    }
  },


  findOneCustom: function * () {
    this.model = model; 
    let data=[];  
    let id = this.params.id; 
    // console.log(this.params.id); 

    try {
 
      // mulai select
      var results = Tutor_teaching_subject.find()
 
      results.where({'id':id})
       
      // populate kategori atas
      results.populate('country')
      results.populate('academic_level')
      results.populate('academic_grade')
      results.populate('course')
       
      // populate user
      results.populate('user')
  
      results = yield results; 
 
      // console.log("length = " + results.length);
      for(var i=0;i<results.length;i++) 
      { 
        
        if(results[i]['user'])
        { 
 
          var temp_user = new Array();
          var temp_tutor_reference = new Array();
          var temp_tutor_working_experience = new Array();
          var temp_tutor_teaching_subject = new Array();
          var temp_tutor_calendar = new Array();
          var temp_user_rate = new Array();
          var temp_lesson = new Array();

          var data_user = new Object();
          var data_tutor_reference = new Object();
          var data_tutor_references = new Object();
          var data_tutor_working_experience = new Object();
          var data_tutor_teaching_subject =new Object();
          var data_tutor_calendar = new Object();
          var data_user_rate = new Object();
          var data_lesson = new Object();

          data_user['role'] = yield Role.find().where({'id':results[i]['user']['role']});    
          data_user['country'] = yield Country.find().where({'id':results[i]['user']['country']});   
          data_user['city']= yield City.find().where({'id':results[i]['user']['city']});    
          data_user['race'] = yield Race.find().where({'id':results[i]['user']['race']});      
          data_user['gender'] = yield Gender.find().where({'id':results[i]['user']['gender']});     
          data_user['religion'] = yield Religion.find().where({'id':results[i]['user']['religion']});     
          data_user['academic_level'] = yield Academic_level.find().where({'id':results[i]['user']['academic_level']});     
          data_user['language'] = yield Language.find().where({'id':results[i]['user']['language']});    
          data_user['payment_method'] = yield Payment_method.find().where({'id':results[i]['user']['payment_method']});  

          data_tutor_reference = yield Tutor_reference.find().where({'user':results[i]['user']['id']});   
          if(data_tutor_reference.length > 0){
            data_tutor_references['id'] = data_tutor_reference[0]['id'];
            data_tutor_references['user'] = data_tutor_reference[0]['user'];
            data_tutor_references['country'] = yield Country.find().where({'id':data_tutor_reference[0]['country']});   
            data_tutor_references['language'] = yield Language.find().where({'id':data_tutor_reference[0]['language']});     
            data_tutor_references['about_me'] = data_tutor_reference[0]['about_me'];
            data_tutor_references['profile_picture'] = data_tutor_reference[0]['profile_picture'];
            data_tutor_references['academic_level'] = yield Academic_level.find().where({'id':data_tutor_reference[0]['academic_level']}); 
            data_tutor_references['major'] = data_tutor_reference[0]['major'];
            data_tutor_references['institute'] = data_tutor_reference[0]['institute'];
            data_tutor_references['cerificate'] = data_tutor_reference[0]['cerificate'];
          }
          data_tutor_working_experience = yield Tutor_working_experience.find().where({'user':results[i]['user']['id']});    
          //data_tutor_teaching_subject = yield Tutor_teaching_subject.find().where({'user':results[i]['user']['id']});    
          data_tutor_calendar = yield Tutor_calendar.find().where({'user':results[i]['user']['id']});  

          data_user_rate = yield User_rate.find().where({'receiver':results[i]['user']['id']}); 
          var count = 0;
          var total = 0;
          var avg = 0;
          count = data_user_rate.length;  
          for(var k=0;k<data_user_rate.length;k++) {   
            total = total + Number(data_user_rate[k]['num_star']);
          }  
          avg = (total / count).toFixed();
          if(isNaN(avg)){
            avg = 0;
          }

          data_lesson['teaching_subject']  = yield Tutor_teaching_subject.count().where({'user':results[i]['user']['id']}); 
          data_lesson['tuition_activity']  = yield Tuition_activity.count().where({'user':results[i]['user']['id']}); 
          
          
           


          // console.log("data_user ");
          // console.log(data_user);
          // console.log("data_tutor_reference ");
          // console.log(data_tutor_reference);
          // console.log(data_tutor_references);
          // console.log("data_tutor_working_experience ");
          // console.log(data_tutor_working_experience);
          // console.log("data_tutor_teaching_subject ");
          // console.log(data_tutor_teaching_subject);
          // console.log("data_tutor_calendar ");
          // console.log(data_tutor_calendar); 
          // console.log("data_user_rate ");
          // console.log(data_user_rate);  
          // console.log("data_lesson"); 
          // console.log(data_lesson); 


          temp_user = {
            "id" : results[i]['user']['id'],
            "role" : data_user['role'][0],
            "active_status" : results[i]['user']['active_status'],
            "first_name" : results[i]['user']['first_name'],
            "last_name" : results[i]['user']['last_name'], 
            "email" : results[i]['user']['email'], 
            "phone_code" : results[i]['user']['phone_code'], 
            "phone_number" : results[i]['user']['phone_number'], 
            "profile_picture" : results[i]['user']['profile_picture'],  
            "country" : data_user['country'][0], 
            "city" : data_user['city'][0], 
            "race" : data_user['race'][0], 
            "gender" : data_user['gender'][0], 
            "religion" : data_user['religion'][0], 
            "academic_level" : data_user['academic_level'][0],  
            "language" : data_user['language'][0], 
            "other_name" : results[i]['user']['other_name'],  
            "birth_date" : results[i]['user']['birth_date'],  
            "passport" : results[i]['user']['passport'],  
            "address" : results[i]['user']['address'],  
            "postal_code" : results[i]['user']['postal_code'],  
            "payment_method" : data_user['payment_method'][0],  
            "bank_name" : results[i]['user']['bank_name'],  
            "account_name" : results[i]['user']['account_name'],  
            "account_number" : results[i]['user']['account_number'],  
          };
 
          // console.log("temp_user ");
          // console.log(temp_user); 
          // console.log(data_tutor_reference.length);
          // console.log(data_tutor_references.length);
          // console.log(Object.keys(data_tutor_references).length);
          // console.log(data_tutor_references);
 
          if(Object.keys(data_tutor_references).length)
          {
            temp_tutor_reference = {
              "id" : data_tutor_references['id'],
              "user" : data_tutor_references['user'],
              "country" : data_tutor_references['country'][0],
              "language" : data_tutor_references['language'][0],
              "about_me" : data_tutor_references['about_me'],  
              "profile_picture" : data_tutor_references['profile_picture'],  
              "academic_level" : data_tutor_references['academic_level'][0], 
              "major" : data_tutor_references['major'], 
              "institute" : data_tutor_references['institute'], 
              "cerificate" : data_tutor_references['certificate'], 
            };    
          }

          // console.log("temp_tutor_reference ")
          // console.log(temp_tutor_reference);

          if(data_tutor_working_experience.length > 0){

            for(var j=0;j<data_tutor_working_experience.length;j++) {

              var temp_tutor_working_experience2 = {
                "id" : data_tutor_working_experience[0]['id'],
                "user" : data_tutor_working_experience[0]['user'],
                "year" : data_tutor_working_experience[0]['year'],
                "experience" : data_tutor_working_experience[0]['experience'], 
                "note" : data_tutor_working_experience[0]['note'], 
                "verified" : data_tutor_working_experience[0]['verified'],  
              };
              temp_tutor_working_experience.push(temp_tutor_working_experience2);
            }
          }

          // console.log("temp_tutor_working_experience ");
          // console.log(temp_tutor_working_experience);

 
          temp_tutor_teaching_subject = {                 
            "id": results[i]['id'],                                            
            "user": results[i]['user']['id'],                   
            "country": results[i]['country']['id'],         
            "academic_level": results[i]['academic_level']['id'],         
            "academic_grade": results[i]['academic_grade']['id'],         
            "course": results[i]['course']['id'],         
            "fee_per_hour": results[i]['fee_per_hour'],         
            "fee_commition": results[i]['fee_commition'],         
            "fee_earn": results[i]['fee_earn'],         
            "note": results[i]['note'],  
          };

          // console.log("temp_tutor_teaching_subject ");
          // console.log(temp_tutor_teaching_subject);

          if(data_tutor_calendar.length > 0){
            temp_tutor_calendar = {
              "id" : data_tutor_calendar[0]['id'],
              "user" : data_tutor_calendar[0]['user'],
              "available_start" : data_tutor_calendar[0]['available_start'],
              "available_end" : data_tutor_calendar[0]['available_end'], 
              "blocked_start" : data_tutor_calendar[0]['blocked_start'], 
              "blocked_end" : data_tutor_calendar[0]['blocked_end'], 
              "note" : data_tutor_calendar[0]['note'], 
            };
          }

          // console.log("temp_tutor_calendar ");
          // console.log(temp_tutor_calendar); 
 
          temp_user_rate = { 
            "receiver" : results[i]['user']['id'],
            "total" : total,
            "count" : count,
            "avg" : avg,
          };         

          // console.log("temp_user_rate ");
          // console.log(temp_user_rate); 


          temp_lesson = { 
            "user" : results[i]['user']['id'],
            "teaching_subject" : data_lesson['teaching_subject'],
            "tuition_activity" : data_lesson['tuition_activity'], 
          };         

          // console.log("temp_lesson ");
          // console.log(temp_lesson); 



          var array = {  
            "id": results[i]['id'],                        
            "id_tutor_teaching_subject": results[i]['id'],                        
            "user": temp_user,              
            "tutor_reference": temp_tutor_reference,           
            "tutor_working_experience": temp_tutor_working_experience,        
            "tutor_calendar": temp_tutor_calendar,                    
            "country": results[i]['country'],         
            "academic_level": results[i]['academic_level'],         
            "academic_grade": results[i]['academic_grade'],         
            "course": results[i]['course'],         
            "user_rate": temp_user_rate,         
            "lesson": temp_lesson,         
            "fee_per_hour": results[i]['fee_per_hour'],         
            "fee_commition": results[i]['fee_commition'],         
            "fee_earn": results[i]['fee_earn'],         
            "note": results[i]['note'],  
          };
          data.push(array);
        }
      } 

      this.body = data[0];

    } catch (err) {
      this.body = err;
    }
  }
};