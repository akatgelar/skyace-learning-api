'use strict';

const nodemailer = require('nodemailer');

module.exports = {

  /**
   * Send an email.
   */

  send: function * () {
    try {
      const email = yield strapi.api.email.services.email.send(this.request.body);
      console.log(email);
      this.body = email;
    } catch (err) {
      console.log(err);
      this.status = err && err.status || 500;
      this.body = err;
    }
  },

  sendCustom: function * (mailOptions2) {

    console.log("option "+mailOptions2);
    console.log("this "+this.params);
    try {
      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing
      //nodemailer.createTestAccount((err, account) => {

          // create reusable transporter object using the default SMTP transport
          let transporter = nodemailer.createTransport({
              host: 'smtp.gmail.com',
              port: 465,
              secure: true, 
              auth: {
                  user: 'elearningnet.sg@gmail.com', // generated ethereal user
                  pass: 'elearningnet2017'  // generated ethereal password
              },
              tls: { 
                  rejectUnauthorized: false 
              }
          });

          // setup email data with unicode symbols
          let mailOptions = {
              from: 'elearningnet.sg@gmail.com', // sender address
              to: 'akatgelar@gmail.com', // list of receivers
              subject: 'Hello ', // Subject line
              text: 'Hello world?', // plain text body
              html: '<b>Hello world?</b>' // html body
          };

          // send mail with defined transport object
          transporter.sendMail(mailOptions2, (error, info) => {
              if (error) {
                  console.log("ada error ::: " + error);
                  return console.log(error);
              }
              console.log('Message sent: %s', info.messageId);
              // Preview only available when sending through an Ethereal account
              console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

              // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
              // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
          });
      //});
    } catch (err) {
      this.status = err && err.status || 500;
      this.body = err;
      console.log(err);
    }
  }
};
